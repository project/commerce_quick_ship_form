<?php

namespace Drupal\commerce_quick_ship_form\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_shipping\ShippingOrderManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce\EntityHelper;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\physical\Weight;
use Drupal\physical\WeightUnit;
use Drupal\commerce_shipping\ShipmentItem;

/**
 * Provides the form for adding countries.
 */
class QuickShipForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The shipping order manager.
   *
   * @var \Drupal\commerce_shipping\ShippingOrderManagerInterface
   */
  protected $shippingOrderManager;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * A request stack symfony instance.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new QuickShipForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_shipping\ShippingOrderManagerInterface $shipping_order_manager
   *   The shipping order manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ShippingOrderManagerInterface $shipping_order_manager, ModuleHandlerInterface $module_handler, MessengerInterface $messenger, RequestStack $requestStack) {
    $this->entityTypeManager = $entity_type_manager;
    $this->shippingOrderManager = $shipping_order_manager;
    $this->moduleHandler = $module_handler;
    $this->messenger = $messenger;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('commerce_shipping.order_manager'),
      $container->get('module_handler'),
      $container->get('messenger'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_quick_ship_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $form_state->getBuildInfo()["args"][0];
    if (!$this->shippingOrderManager->hasShipments($order)) {
      return;
    }

    $form['quick_shipment'] = [
      '#type' => 'fieldset',
      '#title' => $this->t("Quick Shipment"),
      '#weight' => 0,
      '#prefix' => '<div id="edit-tracking-shipments">',
      '#suffix' => '</div>',
    ];
    $form['quick_shipment']['shipment_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t("Shipment settings"),
      '#weight' => 0,
    ];
    $form['quick_shipment']['shipment_fieldset']['shipment_setting'] = [
      '#type' => 'radios',
      '#title' => $this->t('Shipment interaction'),
      '#options' => [
        'edit_shipment' => $this->t('Edit an existing shipment'),
        'create_new' => $this->t('Create a new shipment'),
      ],
      '#required' => TRUE,
      '#default_value' => 'edit_shipment',
      '#ajax' => [
        'callback' => [$this, 'myAjaxCallback'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'edit-tracking-shipments',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying entry...'),
        ],
      ],
    ];

    $create_new = FALSE;
    if (!empty($form_state->getUserInput()['shipment_setting'])) {
      if ($form_state->getUserInput()["shipment_setting"] == 'create_new') {
        $create_new = TRUE;
      }
    }
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
    $shipments = $order->get('shipments')->referencedEntities();
    $shipment_ids = EntityHelper::extractIds($shipments);
    $shipment_labels = EntityHelper::extractLabels($shipments);
    $shipment_options = array_combine($shipment_ids, $shipment_labels);
    $form['quick_shipment']['shipment_fieldset']['selected_shipment'] = [
      '#type' => 'select',
      '#options' => $shipment_options,
      '#title' => $this->t('Shipment'),
      '#description' => $this->t('The shipment to edit.'),
      '#default_value' => end($shipment_ids),
      '#ajax' => [
        'callback' => [$this, 'myAjaxCallback'],
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'edit-tracking-shipments',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying entry...'),
        ],
      ],
    ];

    // Use the default value from the selected_shipment form.
    // If there is user input, use that.
    $sid = $form["quick_shipment"]['shipment_fieldset']["selected_shipment"]["#default_value"];
    if (!empty($form_state->getUserInput()["selected_shipment"])) {
      $sid = $form_state->getUserInput()["selected_shipment"];
    }
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipment = $this->entityTypeManager->getStorage('commerce_shipment')->load($sid);
    /** @var \Drupal\commerce_shipping\ShipmentStorageInterface $shipment_storage */
    $shipment_storage = $this->entityTypeManager->getStorage('commerce_shipment');
    // Get all of the shipments for the current order.
    $order_shipments = $shipment_storage->loadMultipleByOrder($order);
    // Store order_items that are already tied to shipments on this order.
    $already_on_shipment = [];
    foreach ($order_shipments as $order_shipment) {
      $shipment_items = $order_shipment->getItems();
      foreach ($shipment_items as $shipment_item) {
        $order_item_id = $shipment_item->getOrderItemId();
        $already_on_shipment[$order_item_id] = $already_on_shipment[$order_item_id] ?? 0;
        $already_on_shipment[$order_item_id] += $shipment_item->getQuantity();
      }
    }
    $shipment_item_options = [];
    // Populates the default values by looking at the items already in this
    // shipment.
    $shipment_item_defaults = [];
    $shipment_items = $shipment->getItems();
    /** @var \Drupal\commerce_shipping\ShipmentItem $shipment_item */
    foreach ($shipment_items as $shipment_item) {
      $shipment_item_id = $shipment_item->getOrderItemId();
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->entityTypeManager->getStorage('commerce_order_item')->load($shipment_item_id);
      $shipment_item_defaults[$shipment_item_id] = $shipment_item_id;
      $shipment_item_options[$shipment_item_id] = [
        'checked' => TRUE,
        'label' => $shipment_item->getTitle(),
        'ordered' => floatval($order_item->getQuantity()),
        'shipped' => $already_on_shipment[$shipment_item_id] ?? 0,
        'quantity' => floatval($shipment_item->getQuantity()),
      ];
    }
    $form['quick_shipment']['shipment_fieldset']['set_items'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Edit items for this shipment'),
    ];
    $form['quick_shipment']['shipment_fieldset']['shipment_item_fieldset'] = [
      '#type' => 'fieldset',
    ];
    $form['quick_shipment']['shipment_fieldset']['shipment_item_fieldset']['shipment_items'] = [
      '#type' => 'table',
      '#header' => [
        'checked' => '',
        'label' => $this->t('Label'),
        'ordered' => $this->t('Total ordered'),
        'shipped' => $this->t('Shipped'),
        'quantity' => $this->t('Quantity'),
      ],
      '#empty' => $this->t('There are no items yet.'),
      '#states' => [
        'visible' => [
          [
            ':input[name="set_items"]' => ['checked' => TRUE],
          ],
          'or',
          [
            ':input[name="shipment_setting"]' => ['value' => 'create_new'],
          ],
        ],
      ],
    ];

    $order_items = $order->getItems();
    foreach ($order_items as $order_item) {
      // Skip shipment items that are already on this shipment.
      if (isset($shipment_item_options[$order_item->id()]) ||
        !$order_item->hasField('purchased_entity') ||
        in_array($order_item->id(), $already_on_shipment, TRUE)) {
        continue;
      }
      // Items must have a purchasable entity and implement the shippable trait.
      $purchasable_entity = $order_item->getPurchasedEntity();
      if (!empty($purchasable_entity) && $purchasable_entity->hasField('weight')) {
        $shipment_item_options[$order_item->id()] = [
          'checked' => FALSE,
          'label' => $order_item->label(),
          'ordered' => floatval($order_item->getQuantity()),
          'shipped' => $already_on_shipment[$order_item->id()] ?? 0,
          // I guess get the order item quantity. Not sure it matters.
          'quantity' => floatval($order_item->getQuantity()),
        ];
      }
    }
    foreach ($shipment_item_options as $key => $value) {
      // Check any items where the shipped is less than the order item quantity.
      $needs_shipped = FALSE;
      $quantity_needed = $value['quantity'];
      if ($value['ordered'] > $value['shipped']) {
        $needs_shipped = TRUE;
        $quantity_needed = $value['ordered'] - $value['shipped'];
      }
      $form['quick_shipment']['shipment_fieldset']['shipment_item_fieldset']['shipment_items'][$key] = [
        'checked' => [
          '#type' => 'checkbox',
          // Use #value instead of #default_value for AJAX.
          // See: https://drupal.stackexchange.com/a/237055/93997
          '#value' => $create_new ? $needs_shipped : $value['checked'],
        ],
        'label' => ['#plain_text' => $value['label']],
        'ordered' => ['#plain_text' => $value['ordered']],
        'shipped' => ['#plain_text' => $value['shipped']],
        'quantity' => [
          '#type' => 'textfield',
          // Use #value instead of #default_value for AJAX.
          '#value' => $create_new ? $quantity_needed : $value['quantity'],
          '#size' => 1,
          '#attributes' => [
            'type' => 'number',
            'min' => 0,
          ],
        ],
      ];
    }
    $form['quick_shipment']['tracking_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t("Tracking information"),
      '#weight' => 10,
    ];
    $form['quick_shipment']['tracking_fieldset']['tracking_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tracking code'),
      '#maxlength' => 30,
      // Use #value instead of #default_value for AJAX.
      '#value' => $create_new ? '' : $shipment->getTrackingCode(),
    ];
    if ($this->moduleHandler->moduleExists('commerce_shipping_carrier')) {
      $carriers = $this->entityTypeManager->getStorage('commerce_shipping_carrier')->loadMultiple();
      $form['quick_shipment']['tracking_fieldset']['shipping_carrier'] = [
        '#type' => 'select',
        '#options' => EntityHelper::extractLabels($carriers),
        '#title' => $this->t('Shipping Carrier'),
        '#description' => $this->t('The carrier'),
        '#empty_option' => $this->t('- Select -'),
        // Use #value instead of #default_value for AJAX.
        '#value' => $create_new ? '' : $shipment->get('shipping_carrier')->target_id ?? '',
      ];
    }
    $form['quick_shipment']['shipment_state'] = [
      '#type' => 'radios',
      '#title' => $this->t('Shipment state'),
      '#options' => [
        'none' => $this->t('N/A'),
        'finalize_shipment' => $this->t('Finalize shipment'),
        'send_shipment' => $this->t('Send shipment (email).'),
      ],
      '#required' => FALSE,
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => 'none',
      '#weight' => 15,
    ];
    $form['quick_shipment']['actions']['#type'] = 'actions';
    $form['quick_shipment']['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#default_value' => $this->t('Save'),
    ];
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function myAjaxCallback(array $form, FormStateInterface $form_state) {
    // Just return the parent fieldset.
    return $form['quick_shipment'];

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $request = $this->requestStack->getCurrentRequest();
    $values = $form_state->getValues();
    $shipment_state = $values['shipment_state'];
    // Input works better with #values and AJAX.
    $input = $form_state->getUserInput();
    $tracking_code = $input["tracking_code"];
    if ($this->moduleHandler->moduleExists('commerce_shipping_carrier')) {
      $shipping_carrier = $input["shipping_carrier"];
    }

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $form_state->getBuildInfo()["args"][0];

    // Only edit shipments if there are shipments on the order.
    if (!$this->shippingOrderManager->hasShipments($order)) {
      return;
    }
    $shipments = $order->get('shipments')->referencedEntities();
    if ($values["shipment_setting"] === "edit_shipment") {
      $sid = $values["selected_shipment"];
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      $shipment = $this->entityTypeManager->getStorage('commerce_shipment')->load($sid);
      $shipment->set('tracking_code', $tracking_code);
      if ($this->moduleHandler->moduleExists('commerce_shipping_carrier')) {
        $shipment->set('shipping_carrier', $shipping_carrier);
      }
      $shipment->save();
      if ($values['set_items'] == TRUE) {
        // Borrowed from commerce_shipping ShipmentForm.php.
        $this->addShippingItems($form, $form_state, $shipment);
        $shipment->setData('owned_by_packer', FALSE);
        $shipment->save();
      }
      $this->messenger->addMessage($this->t('@title has been updated.', [
        '@title' => $shipment->getTitle(),
      ]));
      $request->query->remove('destination');
    }

    $last_shipment = end($shipments);
    $title = $this->t('Shipment #@number', [
      '@number' => (count($shipments) + 1),
    ]);

    if ($values["shipment_setting"] === "create_new") {
      $shipment = Shipment::create([
        'type' => 'default',
        'order_id' => $order->id(),
        // These need to be sorted for orders with no shipment.
        'package_type' => $last_shipment->get('package_type')->value,
        'shipping_method' => $last_shipment->getShippingMethodId(),
        'shipping_service' => $last_shipment->getShippingService(),
        'shipping_profile' => $last_shipment->getShippingProfile(),
        'title' => $title,
        'amount' => $last_shipment->getAmount(),
        'items' => $last_shipment->getItems(),
        'tracking_code' => $tracking_code,
      ]);
      if ($this->moduleHandler->moduleExists('commerce_shipping_carrier')) {
        $shipment->set('shipping_carrier', $shipping_carrier);
      }

      if ($values['set_items'] == TRUE) {
        // Borrowed from commerce_shipping ShipmentForm.php.
        $this->addShippingItems($form, $form_state, $shipment);
        $shipment->setData('owned_by_packer', FALSE);
        $shipment->save();
      }

      // Make sure the shipment gets added to the order.
      $order = $shipment->getOrder();
      if (!$order) {
        return;
      }
      $order_shipments = $order->get('shipments');
      $shipment_exists = FALSE;
      $save_order = FALSE;
      // Loop over the order shipments to make sure this
      // shipment exists.
      foreach ($order_shipments->getValue() as $order_shipment) {
        if ($order_shipment['target_id'] == $shipment->id()) {
          $shipment_exists = TRUE;
        }
      }
      // Add the shipment to the order if it doesn't exist.
      if (!$shipment_exists) {
        $order_shipments->appendItem($shipment);
        $save_order = TRUE;
      }
      // Save the parent order if the shipment was appended to the order.
      if ($save_order) {
        $order->save();
      }
      $this->messenger->addMessage($this->t('@title has been created.', [
        '@title' => $shipment->getTitle(),
      ]));
      $request->query->remove('destination');
    }
    if ($shipment_state == 'send_shipment') {
      $shipment->set('state', 'shipped');
      $shipment->save();
    }

  }

  /**
   * Borrowed from commerce_shipping ShipmentForm.php.
   *
   * @param array $form
   *   A nested array of form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   */
  protected function addShippingItems(array &$form, FormStateInterface $form_state, ShipmentInterface $shipment) {
    // Clear the shipping items to make sure the list is fresh when we add them.
    $shipment->setItems([]);
    $input = $form_state->getUserInput();
    /** @var \Drupal\commerce_shipping\ShipmentItem $shipment_item */
    foreach ($input['shipment_items'] as $key => $value) {
      $quantity = $value['quantity'];
      if (!empty($value['checked']) != 1 || empty($quantity)) {
        // The item was not included in the shipment.
        continue;
      }
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->entityTypeManager->getStorage('commerce_order_item')->load($key);
      $purchased_entity = $order_item->getPurchasedEntity();

      if ($purchased_entity->get('weight')->isEmpty()) {
        $weight = new Weight(1, WeightUnit::GRAM);
      }
      else {
        /** @var \Drupal\physical\Plugin\Field\FieldType\MeasurementItem $weight_item */
        $weight_item = $purchased_entity->get('weight')->first();
        $weight = $weight_item->toMeasurement();
      }
      $shipment_item = new ShipmentItem([
        'order_item_id' => $order_item->id(),
        'title' => $purchased_entity->label(),
        'quantity' => $quantity,
        'weight' => $weight->multiply($quantity),
        'declared_value' => $order_item->getTotalPrice(),
      ]);
      $shipment->addItem($shipment_item);
    }
  }

}
